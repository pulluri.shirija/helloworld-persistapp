/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "helloworld-persist-app.h"

#include <mildenhall/mildenhall.h>

struct _HlwPersistApp
{
  GApplication parent;

  ClutterActor *stage;                  /* owned */

  MildenhallButtonSpeller *button;      /* unowned */

  gchar *state_file_path;               /* owned */
};

G_DEFINE_TYPE (HlwPersistApp, hlw_persist_app, G_TYPE_APPLICATION)

enum
{
  COLUMN_TEXT = 0,
  COLUMN_LAST = COLUMN_TEXT,
  N_COLUMN
};

static void
button_pressed_cb (MildenhallButtonSpeller *button,
                   gpointer user_data)
{
  g_autofree gchar *display_text = NULL;
  gint count = 0;
  g_autoptr (GError) error = NULL;
  g_autoptr (GKeyFile) key_file = g_key_file_new ();

  HlwPersistApp *self = HLW_PERSIST_APP (user_data);

  /* Load last updated time and context from perstent db
   * However, it's usually very expensive operation so
   * it shouldn't load and store too frequently.
   */
  if (!g_key_file_load_from_file (key_file, self->state_file_path,
      G_KEY_FILE_KEEP_COMMENTS,
      &error))
    {
      g_debug ("Failed to load key file[%s] (reason: %s)",
          self->state_file_path, error->message);
      g_clear_error (&error);

      goto store_db;
    }

  count = g_key_file_get_integer (key_file,
      "Button Event", "count",
      &error);
  if (error)
    {
      g_debug ("Failed to read 'count' (reason: %s)", error->message);
    }

  display_text = g_strdup_printf ("Count: %d", count);
  v_button_speller_set_text (CLUTTER_ACTOR (self->button), display_text);

store_db:
  g_key_file_set_integer (key_file, "Button Event",
      "count", count + 1);

  if (!g_key_file_save_to_file (key_file,
      self->state_file_path,
      &error))
    {
      g_warning ("Failed to save db (reason: %s)", error->message);
    }
}

static gboolean
on_stage_destroy (ClutterActor *stage,
                  gpointer user_data)
{
  GApplication *app = user_data;

  g_debug ("The main stage is destroyed.");

  /* chain up */
  CLUTTER_ACTOR_GET_CLASS (stage)->destroy (stage);

  /* The main window is detroyed (closed) so the application
   * needs to be released */
  g_application_release (app);

  return TRUE;
}

static void
startup (GApplication *app)
{
  g_autoptr (GObject) widget_object = NULL;
  g_autoptr (ThornburyItemFactory) item_factory = NULL;
  ThornburyModel *model;
  g_autofree gchar *widget_properties_file = NULL;

  HlwPersistApp *self = HLW_PERSIST_APP (app);

  g_application_hold (app);

  /* chain up */
  G_APPLICATION_CLASS (hlw_persist_app_parent_class)->startup (app);

  /* All persistent data should be located in XDG_DATA_HOME.
   * When the agent or app is launched by 'canterbury-exec',
   * the variable will be properly set. e.g.
   * /var/Applications/org.apertis.HelloWorld/users/1000/share */
  self->state_file_path = g_strdup_printf ("%s/%s.ini",
      g_get_user_data_dir (),
      g_application_get_application_id (app));

  /* build ui components */
  self->stage = mildenhall_stage_new (g_application_get_application_id (app));

  g_signal_connect (G_OBJECT (self->stage),
      "destroy", G_CALLBACK (on_stage_destroy),
      self);

  widget_properties_file = g_build_filename (PKGDATADIR,
                                             "button_speller_text_prop.json",
                                             NULL);

  item_factory = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_TYPE_BUTTON_SPELLER,
      widget_properties_file);

  g_object_get (item_factory,
                "object", &widget_object,
                NULL);

  self->button = MILDENHALL_BUTTON_SPELLER (widget_object);

  model = THORNBURY_MODEL (
      thornbury_list_model_new (N_COLUMN,
                                G_TYPE_STRING,
                                NULL,
                                -1));

  thornbury_model_append (model, 
                          COLUMN_TEXT, "Add Count!",
                          -1);

  g_object_set (self->button,
                "model", model,
                NULL);

  g_object_unref (model);

  clutter_actor_add_child (self->stage, CLUTTER_ACTOR (self->button));

  g_signal_connect (self->button,
      "button-press", G_CALLBACK (button_pressed_cb),
      self);
}

static void
activate (GApplication *app)
{
  HlwPersistApp *self = HLW_PERSIST_APP (app);

  clutter_actor_show (self->stage);
}

static void
hlw_persist_app_dispose (GObject *object)
{
  HlwPersistApp *self = HLW_PERSIST_APP (object);

  g_clear_pointer (&self->stage, clutter_actor_destroy);
  g_clear_pointer (&self->state_file_path, g_free);

  G_OBJECT_CLASS (hlw_persist_app_parent_class)->dispose (object);
}

static void
hlw_persist_app_class_init (HlwPersistAppClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = hlw_persist_app_dispose;

  app_class->startup = startup;
  app_class->activate = activate;
}

static void
hlw_persist_app_init (HlwPersistApp *self)
{
}
